import json

from libs.api_wrapper import api_post, api_delete, api_get, api_put
from libs.wrappers import *
from settings import *


def get_redash_ds(redash_ds_id: int, idtoken: str, tenant_id: str):
    return api_get('%s/%s' % (REDASH_ENDPOINT, tenant_id), '/api/data_sources/%s' % redash_ds_id, idtoken=idtoken)


def create_redash_ds(payload: dict, idtoken: str, tenant_id: str):
    return api_post('%s/%s' % (REDASH_ENDPOINT, tenant_id), '/api/data_sources', payload, idtoken=idtoken)


def update_redash_ds(redash_ds_id: int or str, payload: str, idtoken: str, tenant_id: str):
    return api_post('%s/%s' % (REDASH_ENDPOINT, tenant_id), '/api/data_sources/%s' % redash_ds_id, payload, idtoken=idtoken)


def delete_redash_ds(redash_ds_id: str, idtoken, tenant_id: str):
    return api_delete('%s/%s' % (REDASH_ENDPOINT, tenant_id), '/api/data_sources/{}'.format(redash_ds_id),
                      idtoken=idtoken)


# @check_tenant_ds_mapping_before_create_query_id
# @save_tenant_query_mapping_after_creating_query_id
def create_query(redash_ds_id, query: str, tenant_id, idtoken):
    payload = {"data_source_id": int(redash_ds_id),
               "max_age": 0,
               "parameters": {},
               "query": query}
    return api_post('%s/%s' % (REDASH_ENDPOINT, tenant_id), '/api/query_results',
                    payload=json.dumps(payload),
                    idtoken=idtoken)


# @check_tenant_query_mapping_before_poll
def poll_query(query_id: str, tenant_id, idtoken):
    return api_get('%s/%s' % (REDASH_ENDPOINT, tenant_id), '/api/jobs/{}'.format(query_id), idtoken=idtoken)


def get_query_result(result_id: str, idtoken, tenant_id: str):
    return api_get('%s/%s' % (REDASH_ENDPOINT, tenant_id), '/api/query_results/{}'.format(result_id), idtoken=idtoken)


# To get the data source name from redash
def get_data_source(ds_id: str, tenant_id, idtoken):
    return api_get('%s/%s' % (REDASH_ENDPOINT, tenant_id), '/api/data_sources/{}'.format(ds_id), idtoken=idtoken)
