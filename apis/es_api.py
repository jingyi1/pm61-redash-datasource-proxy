import json

from settings import DISPATCHER_ENDPOINT
from libs.api_wrapper import *


def create_es_datasource(payload: bytes or str, Idtoken: str):
    return api_post(DISPATCHER_ENDPOINT, '/source', payload, Idtoken)


def delete_es_datasource(datasource_id: str, Idtoken: str):
    return api_delete(DISPATCHER_ENDPOINT, '/source/{}?force=true'.format(datasource_id), None, Idtoken)


def get_es_datasource(datasource_id: str, Idtoken: str):
    return api_get(DISPATCHER_ENDPOINT, '/source/{}'.format(datasource_id), idtoken=Idtoken)


def update_es_datasource(datasource_id: str, payload: bytes or str, Idtoken: str):
    return api_put(DISPATCHER_ENDPOINT, '/source/{}'.format(datasource_id), payload=payload, idtoken=Idtoken)


def save_es_mapping(es_ds_id: str, redash_ds_id: str, Idtoken: str):
    payload = {
        "redashDataSourceId": str(redash_ds_id)
    }
    return api_put(DISPATCHER_ENDPOINT, '/source/{}'.format(es_ds_id), payload=json.dumps(payload), idtoken=Idtoken)
