from libs.api_wrapper import api_get
from settings import *


def get_permissions(Idtoken: str):
    return api_get(PERMISSION_ENDPOINT, 'user/permissions', idtoken=Idtoken)
