import os
import boto3
from dotenv import load_dotenv

load_dotenv()

REDASH_ENDPOINT = os.environ.get('REDASH_ENDPOINT', 'https://vitesse-redash.pm61data.io')
DISPATCHER_ENDPOINT = os.environ.get('DISPATCHER_ENDPOINT', 'https://tutwh1u3ze.execute-api.us-east-1.amazonaws.com/dev/')
PERMISSION_ENDPOINT = os.environ.get('PERMISSION_ENDPOINT', 'https://52a0qe23ua.execute-api.us-west-1.amazonaws.com/prod/api/v3/')
VITESSE_DS_TABLE = os.environ.get('VITESSE_DS_TABLE', 'vitesse_datasource')
VITESSE_QUERY_TABLE = os.environ.get('VITESSE_QUERY_TABLE', 'vitesse_query')
BEANSTALK_BASE_URL = os.environ.get('BEANSTALK_BASE_URL', ' https://tlkpyfiub0.execute-api.us-east-2.amazonaws.com/prod/')
X_API_KEY = os.environ.get('X_API_KEY', '4DWWUxVvMB9PNSW8aXyAw6U4qnCRFU1g4Q0VOrib')
Dynamodb_instance = boto3.client('dynamodb', region_name='us-east-1')

