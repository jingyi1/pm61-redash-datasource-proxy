import requests
from libs.response_wrapper import create_response, cors_headers
from libs.util import get_tenant_id, get_idToken
from settings import *


def proxy(event, context):
    tenant_id = get_tenant_id(event)
    idtoken = get_idToken(event)
    path = event['path']
    redash_url = None

    print(event)


    if path == "/setup":
        redash_url = '%s' % REDASH_ENDPOINT + path
    else:
        redash_url = '%s/%s' % (REDASH_ENDPOINT, tenant_id) + path



    method = event['httpMethod']
    headers = {
        'idToken': idtoken,
        'Content-Type': 'application/json'
    }
    data = event['body']

    if data:
        data = data.encode('utf-8')

    request_method = getattr(requests, method.lower())
    try:
        res = request_method(redash_url, data=data, headers=headers)

    except Exception as e:
        return create_response(500, cors_headers, str(e))
    if res.status_code >= 400:
        body = {
            "error": res.reason,
            # "stackTrace on Redash": res.json()
        }
        return create_response(res.status_code, cors_headers, body)
    return create_response(res.status_code, cors_headers, res.json())
