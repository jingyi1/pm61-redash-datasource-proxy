
sample_payload = {
    "port": 8080,
    "supportsSchemas": True,
    "retrieveForeignKeys": True,
    "pageSize": -1,
    "tableNamePattern": "null",
    "supportsCatalogs": True,
    "identifierQuoteString": "\"",
    "topic": [],
    "type": "Presto",
    "name": "test_case_presto",
    "host": "10.201.1.101",
    "catalogName": "hive",
    "databaseName": "pm61devs3",
    "userName": "123123",
    "description": "description"
}