from unittest import TestCase
from apis.es_api import get_es_datasource
from datasource_handler import _create_datasource
from models.datasource import *
from tests.configs import token
from tests.fixture import data



class TryTesting(TestCase):

    es_id = ""
    def test_source_model(self):
        assert (True)
        tmp = "{\"supportsSchemas\":true,\"retrieveForeignKeys\":true,\"pageSize\":-1,\"tableNamePattern\":\"null\"," \
              "\"supportsCatalogs\":true,\"identifierQuoteString\":\"\",\"tableTypes\":{\"TABLE\":\"VIEW\"}," \
              "\"type\":\"MySQL\",\"name\":\"mysql_vitesse_1\"," \
              "\"host\":\"toymysql-pm61qa.c1mlnd517jhj.us-west-1.rds.amazonaws.com\",\"port\":\"3306\"," \
              "\"userName\":\"promethium\",\"password\":\"Promethium2019\",\"databaseName\":\"promethium\"}"
        x = format_source_model(tmp)
        print(x)

    def test_get_es_ds_by_id(self):
        id = "ccda1346-a813-4b6d-be28-dac9115789b8";
        x = get_es_datasource(datasource_id=id, Idtoken=token)

        print(x)
        assert (True)

    def test_create_datasource(self):
        event = {
            'headers': {
                'idtoken': token,
            },
            'body': json.dumps(data.sample_payload)
        }
        response = _create_datasource(event=event, context=None)

        print(response)

        assert True

    def test_delete_datasource(self):
        print(self.es_id);
        assert True

