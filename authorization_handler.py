import os
import jwt


def allow_policy(method_arn):
    return {
        "principalId": "apigateway.amazonaws.com",
        "policyDocument": {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Action": "execute-api:Invoke",
                    "Effect": "Allow",
                    "Resource": '*'
                }
            ]
        }
    }


def deny_all_policy():
    return {
        "principalId": "*",
        "policyDocument": {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Action": "*",
                    "Effect": "Deny",
                    "Resource": "*"
                }
            ]
        }
    }


def authorize(event, context):
    encoded_str = event.get('authorizationToken')

    # TODO what policy will we enforce at proxy level ?

    try:
        tenant = jwt.decode(encoded_str, verify=False)['custom:tenant_id']
        if not tenant:
            print('tenant is missing')
            return deny_all_policy()
        return allow_policy(event['methodArn'])

    except Exception as e:
        print('exception thrown, api denied', e)
        return deny_all_policy()
