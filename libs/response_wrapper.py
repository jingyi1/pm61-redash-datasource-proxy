import json

cors_headers = {
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Credentials": True
}


def create_response(status_code, header, body):
    return {
        "statusCode": status_code,
        "headers": header,
        "body": json.dumps(body)
    }


def wrap_response(content: dict or str, status_code: str or int):
    return {
        "statusCode": status_code,
        "headers": {
            "Content-Type": "application/json",
            "access-control-allow-credentials": "true",
            "access-control-allow-origin": "*"

        },
        "body": json.dumps(content)
    }


def is_response_success(status: str or int) -> bool:
    return int(status) == 200 or int(status) == 201 or int(status) == 204
