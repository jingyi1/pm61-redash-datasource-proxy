from typing import Tuple
import requests


def api_get(endpoint, path, idtoken=None, api_key=None):
    headers = None
    if idtoken:
        headers = {'idtoken': '{}'.format(idtoken)}
    url = endpoint+path
    raw_resp = requests.get(url, headers=headers)
    print(url, raw_resp)
    return int(raw_resp.status_code), raw_resp.json()


def api_post(endpoint: str, path: str, payload: str = None, idtoken: str = None) -> Tuple[int, dict]:
    headers = None
    if idtoken:
        headers = {'idtoken': '{}'.format(idtoken)}

    url = endpoint+path
    print(url, payload)
    raw_resp = requests.post(url, data=payload, headers=headers)
    print(raw_resp.status_code, raw_resp.json())
    return int(raw_resp.status_code), raw_resp.json()


def api_delete(endpoint: str, path: str, payload: str = None, idtoken=None, api_key=None):
    headers = None
    if idtoken:
        headers = {'idtoken': '{}'.format(idtoken)}

    url = endpoint+path
    raw_resp = requests.delete(url, data=payload, headers=headers)
    print(url, raw_resp.status_code, raw_resp.text)
    return int(raw_resp.status_code), raw_resp.text


def api_put(endpoint: str, path: str, payload: str = None, idtoken: str = None, api_key: str = None):
    headers = None
    if idtoken:
        headers = {'idtoken': '{}'.format(idtoken)}

    url = endpoint+path
    raw_resp = requests.put(url, data=payload, headers=headers)
    return int(raw_resp.status_code), raw_resp.json()
