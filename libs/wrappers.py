from libs.dynamodb_handler import *
from settings import Dynamodb_instance
from typing import Tuple


def check_tenant_ds_mapping_before_create_query_id(func) -> [str or dict, int]:
    def wrapper(ds_id, query, tenant_id, idtoken):
        entry = find_ds_entry(tenant_id=tenant_id, redash_ds_id=ds_id, dynamoClient=Dynamodb_instance)
        if entry is not None:
            status, resp = func(ds_id, query, tenant_id)
            if int(status) != 200:
                raise ValueError({"error": 'Error creating query on redash'})
            return status, resp

        else:
            raise ValueError({"error": 'this tenant is not authorized to visit this datasource'})

    return wrapper


def save_tenant_query_mapping_after_creating_query_id(func) -> [str or dict, int]:
    def wrapper(ds_id, query, tenant_id, idtoken):
        status, resp = func(ds_id, query, tenant_id)
        save_query(tenant_id=tenant_id, query_id=resp['job']['id'],
                   dynamoClient=Dynamodb_instance)
        return status, resp

    return wrapper


def check_tenant_query_mapping_before_poll(func) -> [str or dict, int]:
    def wrapper(query_id, tenant_id):
        entry = find_tenant_queryid_mapping(tenant_id, query_id, Dynamodb_instance)
        if entry is None:
            raise ValueError({"error": "the query id does not belong to your tenant"})
        else:
            return func(query_id, tenant_id)
    return wrapper
