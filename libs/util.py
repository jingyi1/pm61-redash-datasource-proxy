import json

import jwt
import requests
import os
from settings import X_API_KEY, BEANSTALK_BASE_URL


def get_tenant_id(event):
    token = None
    if 'idtoken' in event['headers']:
        token = event['headers']["idtoken"]

    if 'IdToken' in event['headers']:
        token = event['headers']["IdToken"]
    if 'idToken' in event['headers']:
        token = event['headers']["idToken"]

    temp = jwt.decode(token, verify=False)

    if temp['custom:tenant_id']:
        return temp['custom:tenant_id']
    else:
        return temp['cognito:groups'][0]


def get_idToken(event):
    # TODO: simplify
    token = None
    if 'idtoken' in event['headers']:
        token = event['headers']["idtoken"]
    if 'IdToken' in event['headers']:
        token = event['headers']["IdToken"]
    if 'idToken' in event['headers']:
        token = event['headers']["idToken"]

    return token




def update_presto_execution_stats(sql, tenant_id, pm61_ds_name):
    try:
        print("::: Inside update_presto_execution_stats :: ")
        from sql_metadata.parser import Parser
        tables = Parser(sql).tables[0]
        print("::: Parsed Table ::: ", tables)
        table_str = tables.split(".")
        catalog = pm61_ds_name  ##table_str[0]
        schema = table_str[0]
        table = table_str[1]
        query_stats = {
            "redash_id": "NA",
            "table_info": [
                {
                    "catalog": catalog,
                    "schema": schema,
                    "table": table,
                    "authorization": "redash-roxy-user",
                    "filters": [],
                    "columns": [],
                    "directlyReferenced": True
                }
            ],
            "query_id": "20210916_000029_00000_h96ji",
            "catalog": catalog,
            "schema": schema,
            "principal": "ec2-user",
            "user_agent": "python-requests/2.21.0",
            "client_info": "NA",
            "source": "pyhive",
            "environment": "vitesse",
            "server_version": "358-e",
            "usr": "ec2-user",
            "query_state": "FINISHED",
            "query": "",
            "total_rows": 0,
            "total_bytes": 0,
            "output_rows": 0,
            "written_rows": 0,
            "written_bytes": 0,
            "cpu_time_ms": 0,
            "wall_time_ms": 0,
            "queued_time_ms": 0,
            "peak_user_memory_bytes": 0,
            "peak_total_non_revocable_memory_bytes": 0,
            "peak_task_user_memory": 0,
            "peak_task_total_memory": 0,
            "physical_input_bytes": 0,
            "physical_input_rows": 0,
            "internal_network_bytes": 0,
            "internal_network_rows": 0,
            "cumulative_memory": 0,
            "completed_splits": 0,
            "plan_node_stats_and_costs": "{\"stats\":{},\"costs\":{}}",
            "resource_waiting_time": 0,
            "analysis_time": 0,
            "execution_time": 0,
            "create_time": 0,
            "end_time": 0
        }

        updateUrl = BEANSTALK_BASE_URL + "/api/v3/query/stats"
        headers = {
            'content-type': 'application/json',
            'x-api-key': X_API_KEY,
            'tenantid': tenant_id
        }
        print("updateUrl " + str(updateUrl))
        response = requests.post(updateUrl, headers=headers, data=json.dumps(query_stats))
        print(response.text)
    except Exception as e:
        print(e)
        print("::: Exception in updating query stats :::")
