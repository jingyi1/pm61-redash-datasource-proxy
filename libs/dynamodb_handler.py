from settings import *
from botocore.exceptions import ClientError
from typing import Union


def create_tenant_ds_mapping(tenant_id, redash_ds_id, dynamoClient):

    response = dynamoClient.put_item(
        TableName=VITESSE_DS_TABLE,
        Item={
            'tenant_id': {'S': str(tenant_id)},
            'redash_datasource_id': {'S': str(redash_ds_id)}
        }
    )
    return response


def save_query(tenant_id, query_id, dynamoClient):
    return dynamoClient.put_item(
        TableName=VITESSE_QUERY_TABLE,
        Item={
            'tenant_id': {'S': str(tenant_id)},
            'query_id': {'S': str(query_id)},
        }
    )


def find_ds_entry(tenant_id, redash_ds_id, dynamoClient) -> Union[None, dict]:
    try:
        response = dynamoClient.query(
            TableName=VITESSE_DS_TABLE,
            KeyConditionExpression='tenant_id = :tid AND redash_datasource_id = :ds_id',
            ExpressionAttributeValues={
                ':tid': {'S': str(tenant_id)},
                ':ds_id': {'S': str(redash_ds_id)}
            }
        )
    except ClientError as e:
        raise e
    else:
        if not response['Items']:
            return None
        return response['Items'][0]


def find_tenant_queryid_mapping(tenant_id, query_id, dynamoClient) -> Union[dict, None]:
    response = dynamoClient.query(
        TableName=VITESSE_QUERY_TABLE,
        KeyConditionExpression='tenant_id = :tid AND query_id = :qid',
        ExpressionAttributeValues={
            ':tid': {'S': str(tenant_id)},
            ':qid': {'S': str(query_id)}
        }
    )
    if not response['Items']:
        return None
    else:
        return response['Items'][0]


def remove_ds_entry(tenant_id, redash_ds_id, dynamoClient):
    try:
        response = dynamoClient.delete_item(
            TableName=VITESSE_DS_TABLE,
            Key={
                'tenant_id': {'S': str(tenant_id)},
                'redash_datasource_id': {'S': str(redash_ds_id)}
            }
        )
    except ClientError as e:
        print(e)
        raise e
    else:
        return response
