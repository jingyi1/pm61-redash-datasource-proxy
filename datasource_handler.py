from apis.permission_api import *
from apis.redash_api import create_redash_ds, delete_redash_ds, get_redash_ds, update_redash_ds
from libs.dynamodb_handler import create_tenant_ds_mapping, remove_ds_entry
from libs.response_wrapper import wrap_response, is_response_success
from libs.util import *
from models.datasource import *
from apis.es_api import *


def _create_datasource(event, context):
    tenant_id = get_tenant_id(event)
    idtoken = get_idToken(event)
    payload: str = event['body']
    response = {}
    try:
        es_status, es_resp = create_es_datasource(payload, idtoken)
        response = es_resp
        if int(es_status) != 200 and int(es_status) != 201:
            raise ValueError({'error': es_resp, 'status': es_status})

        redash_status, redash_resp = create_redash_ds(format_source_model(payload), idtoken, tenant_id=tenant_id)

        ds_id = str(es_resp['id'])
        redash_ds_id = str(redash_resp['id'])

        if int(redash_status) != 201 and int(redash_status) != 200:
            delete_es_datasource(ds_id, idtoken)
            raise ValueError({'error': "Failure to create datasource in redash", 'status': redash_status})
        save_es_mapping(es_ds_id=ds_id, redash_ds_id=redash_ds_id, Idtoken=idtoken)
        # create_tenant_ds_mapping(tenant_id, redash_ds_id, Dynamodb_instance)
    except ValueError as err:
        print(err.args[0], err.args[0]['status'])
        return wrap_response(err.args[0], err.args[0]['status'])
    else:
        response['redash_ds_id'] = redash_ds_id
        return wrap_response(response, 201)


def _delete_datasource(event, context):
    tenant_id = get_tenant_id(event)
    idtoken = get_idToken(event)
    get_permissions(idtoken)
    ds_id = event['pathParameters']['elastic_datasource_id']
    try:
        sanity_status, sanity_content = get_es_datasource(ds_id, idtoken)

        if int(sanity_status) != 204 and int(sanity_status) != 200:
            raise ValueError({'error': f'{sanity_content} {sanity_status}'})

        if 'redashDataSourceId' not in sanity_content:
            delete_es_datasource(ds_id, idtoken)
            return wrap_response('datasource deleted, but it is not associated with redash ds', 200)

        redash_ds_id = sanity_content['redashDataSourceId']

        es_status, es_content = delete_es_datasource(ds_id, idtoken)
        print(redash_ds_id)

        if int(es_status) != 204 and int(es_status) != 200:
            raise ValueError({'error': f'Failed to Delete DataSource from PM61 {es_content}'})

        redash_status, content = delete_redash_ds(redash_ds_id=redash_ds_id, idtoken=idtoken, tenant_id=tenant_id)

        if int(redash_status) != 204 and int(redash_status) != 200:
            raise ValueError({'error': f'error deleting data source in redash cloud {redash_status}'})

        # remove_ds_entry(tenant_id, redash_ds_id, Dynamodb_instance)

    except ValueError as err:
        return wrap_response(err.args[0], 503)
    else:
        return wrap_response({'elastic_ds_id': ds_id, 'redash_ds_id': redash_ds_id, 'status': 'deleted'}, 204)


def _update_datasource(event, context):
    tenant_id = get_tenant_id(event)
    idtoken = get_idToken(event)
    updated_payload: dict = json.loads(format_source_model(event['body']))
    ds_id = event['pathParameters']['elastic_datasource_id']

    print(updated_payload)

    try:
        es_status, elastic_ds = get_es_datasource(ds_id, idtoken)

        es_update_status, updated_es_ds = update_es_datasource(datasource_id=ds_id,
                                                               payload=event['body'],
                                                               Idtoken=idtoken)

        if not is_response_success(es_status):
            raise ValueError({'error': f'{elastic_ds} {es_status}'}, es_status)

        redash_ds_id = elastic_ds['redashDataSourceId']

        status, redash_ds = get_redash_ds(redash_ds_id=redash_ds_id, idtoken=idtoken, tenant_id=tenant_id);

        print("GET RESULT", elastic_ds, redash_ds)

        if not is_response_success(status):
            raise ValueError({'error': f'{redash_ds} {status}'}, status)

        redash_ds['name'] = updated_payload['name']
        redash_ds['options'] = updated_payload['options']

        status_2, content_2 = update_redash_ds(redash_ds_id=redash_ds_id,
                                               payload=json.dumps(redash_ds),
                                               idtoken=idtoken,
                                               tenant_id=tenant_id)

        if not is_response_success(status_2):
            raise ValueError({'error': f'{content_2} {status_2}'}, status_2)

    except ValueError as err:
        return wrap_response(err.args[0], err.args[0]['status'])

    return wrap_response(
        {
            "status": "updated"
        }, status_2)
