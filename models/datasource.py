import json

unmodified_pattern = '------'
redash_unmodified_pattern = '--------'


def format_source_model(payload: str) -> str:
    payload = json.loads(payload)
    datasource_type = payload['type'].lower()
    try:
        if datasource_type == "snowflake":
            options_mapping = {
                'name': 'name',
                'accountName': 'account',
                'userName': 'user',
                'password': 'password',
                'wareHouse': 'warehouse',
                'databaseName': 'database'
            }
            excluded_keys = ['name']
            if payload['password'] == unmodified_pattern:
                payload['password'] = redash_unmodified_pattern

            return json.dumps({
                "name": payload['name'] if 'name' in payload else "",
                "options": {options_mapping[k]: payload[k] for k in
                            set(list(options_mapping.keys())) - set(excluded_keys)},
                "type": "snowflake",
            })

        if datasource_type == 'postgresql':
            payload['port'] = int(payload['port'])
            options_mapping = {
                'name': 'name',
                'databaseName': 'dbname',
                'host': 'host',
                'password': 'password',
                'port': 'port',
                'userName': 'user'

            }
            excluded_keys = ['name']

            if payload['password'] == unmodified_pattern:
                payload['password'] = redash_unmodified_pattern

            return json.dumps({
                "name": payload['name'] if 'name' in payload else "",
                "options": {options_mapping[k]: payload[k] for k in
                            set(list(options_mapping.keys())) - set(excluded_keys)},
                "type": "pg",
            })

        if datasource_type == "athena":
            options_mapping = {
                'userName': 'aws_access_key',
                'password': 'aws_secret_key',
                's3OutputLocation': 's3_staging_dir',
                'awsRegion': 'region',
                'databaseName': 'schema'
            }
            excluded_keys = ['name']

            if payload['password'] == unmodified_pattern:
                payload['password'] = redash_unmodified_pattern

            return json.dumps({
                "name": payload['name'],
                "options": {options_mapping[k]: payload[k] for k in
                            set(list(options_mapping.keys())) - set(excluded_keys)},
                "type": "athena",
            })

        if datasource_type == "presto":
            payload['port'] = int(payload['port'])

            options_mapping = {
                "name": 'name',
                'host': 'host',
                'databaseName': 'schema',
                'catalogName': 'catalog',
                'userName': 'username',
                'protocol': 'protocol',
                'port': 'port',
            }
            # password login using https protocol is password is is present inside the payload
            if "password" in payload:
                payload['protocol'] = 'https'
                options_mapping = {**options_mapping, 'password': 'password'}
            # password-less login using http protocol is password is not being provided
            else:
                payload['protocol'] = 'http'

            excluded_keys = ['name', 'catalogName', 'databaseName']

            return json.dumps({
                "name": payload['name'],
                "options": {options_mapping[k]: payload[k] for k in
                            set(list(options_mapping.keys())) - set(excluded_keys)},
                "type": "presto",
            })

        # if datasource_type == 'sqlserver':
        #     payload['port'] = int(payload['port'])
        #     options_mapping = {
        #         'name': 'name',
        #         "databaseName": 'db',
        #         'host': 'server',
        #         'userName': 'user',
        #         'password': 'password',
        #         'port': 'port',
        #     }
        #     excluded_keys = ['name']
        #
        #     if payload['password'] == unmodified_pattern:
        #         payload['password'] = redash_unmodified_pattern
        #
        #     return json.dumps({
        #         'name': payload['name'],
        #         "options": {options_mapping[k]: payload[k] for k in
        #                     set(list(options_mapping.keys())) - set(excluded_keys)},
        #         'type': 'mssql'
        #     })

        if datasource_type == 'red_redshift':
            payload['port'] = int(payload['port'])
            options_mapping = {
                'name': 'name',
                'databaseName': 'dbname',
                'host': 'host',
                'port': 'port',
                'user': 'userName',
            }
            excluded_keys = ['name']

            if payload['password'] == unmodified_pattern:
                payload['password'] = redash_unmodified_pattern

            return json.dumps({
                'name': payload['name'],
                'options': {
                    options_mapping[k]: payload[k] for k in
                    set(list(options_mapping.keys()) - set(excluded_keys))},
                'type': 'redshift'
            })

        if datasource_type == 'impala':
            payload['port'] = int(payload['port'])
            excluded_keys = ['name']
            options_mapping = {
                'databaseName': 'database',
                'host': 'host',
                'password': 'password',
                'port': 'port',
                'userName': 'user',
            }
            return json.dumps({
                'name': payload['name'],
                'options': {
                    options_mapping[k]: payload[k] for k in set(list(options_mapping.keys() - set(excluded_keys)))
                },
                'type': 'impala'
            })

        if datasource_type == 'oracle':
            payload['port'] = int(payload['port'])
            excluded_keys = ['name']
            options_mapping = {
                'sid': 'servicename',
                'host': 'host',
                'password': 'password',
                'port': 'port',
                'userName': 'user',
            }
            return json.dumps({
                'name': payload['name'],
                'options': {
                    options_mapping[k]: payload[k] for k in set(list(options_mapping.keys() - set(excluded_keys)))
                },
                'type': 'oracle'
            })

        # -------------------------------------------unsupported do not touch -----------------------------------------
        # unknown inputs

        if datasource_type == 'big_query':
            return json.dumps({})

        # bugged, PM datasource missing password
        if datasource_type == 'salesforce':
            return json.dumps({
                "options": {
                    "password": payload['password'],
                    "token": payload['token'],
                    "username": payload['userName'],
                },
                "type": "salesforce",
                "name": payload['name'],
            })

        if datasource_type == "mysql":
            return json.dumps({
                "options": {
                    "host": payload['host'],
                    "port": int(payload["port"]),
                    "user": payload["userName"],
                    "passwd": payload["password"],
                    "db": payload["databaseName"],
                },
                "type": "mysql",
                "name": payload["name"],
            })

        # unknown rules
        if datasource_type == 'hive':
            return json.dumps({})

    except Exception as err:
        print(err)
        raise ValueError({'error': str(err), 'status': 400})
