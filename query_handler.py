from libs.response_wrapper import wrap_response
from apis.redash_api import *
from libs.util import get_tenant_id, get_idToken, update_presto_execution_stats


def echo(event, context):
    echo_id = event['pathParameters']['echo_id']
    tenant_id = get_tenant_id(event)

    print(VITESSE_DS_TABLE);
    print(VITESSE_QUERY_TABLE)



    if event['httpMethod'] == 'GET':
        return wrap_response({'METHOD': 'GET', "data": event}, 200)
    if event['httpMethod'] == 'POST':
        return wrap_response({"METHOD": "POST", "data": event}, 201)


# manually ensure view-table-data is a prerequisite for view-datamap/edit-datamap
# protect with view_table_data permission
def _generate_query(event, context):
    data = json.loads(event['body'])
    redash_ds_id = data['data_source_id']
    query = data['query']
    tenant_id = get_tenant_id(event)
    idtoken = get_idToken(event)

    try:
        #throw error if redash_ds_id is not existed on redash
        status, resp = create_query(redash_ds_id, query, tenant_id=tenant_id, idtoken=idtoken)

        ## To populate Query stats for Pm61 dashboards, after submitting the query to redash
        redash_status, redash_resp = get_data_source(redash_ds_id, tenant_id, idtoken)
        pm61_ds_name = redash_resp.get("name")
        update_presto_execution_stats(query, tenant_id, pm61_ds_name)
    except ValueError as err:
        return wrap_response(err.args[0], 404)
    else:
        return wrap_response(resp, 200)


def _poll_query(event, context):
    tenant_id = get_tenant_id(event)
    idtoken = get_idToken(event)
    query_id = event['pathParameters']['query_id']
    try:
        status, resp = poll_query(query_id, tenant_id, idtoken= idtoken)
    except ValueError as err:
        return wrap_response(err.args[0], 404)
    else:
        return wrap_response(resp, 200)


def _get_query_result(event, context):
    tenant_id = get_tenant_id(event)
    idtoken = get_idToken(event)
    result_id = event['pathParameters']['result_id']
    status, resp = get_query_result(result_id, tenant_id=tenant_id, idtoken=idtoken)
    return wrap_response(resp, status)
